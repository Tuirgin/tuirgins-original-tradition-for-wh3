With or without surprise the direction and manner of a monster's
approach should be adjudicated by the referee in accordance with its:

- **type**
- **surroundings**
- **disposition of the players**

----

- **Unintelligent monsters** will simply attack.
- **Cunning monsters** will judge the situation accordingly.
- **Chaotics** are predisposed to attack **lawfuls**, and vice versa.
- **Normal man-types** will only attack 4^th^ (or higher) level fighters if there are no other targets.

----

The referee can otherwise determine monster behavior according to the
following table, adjusting any result for:

- **bribes** offered
- perceived **threats**
- differences of **race** or **alignment**
- and so on...
